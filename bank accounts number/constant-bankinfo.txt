Association pour les Arts et les Médias
Rue du Fort 5 
1060 Bruxelles – Belgique
KBO/BCE: 0858.363.787
BTW NVT /TVA NA
T/F ++ 32 (0)2 539 24 67
E info@constantvzw.org
W www.constantvzw.org
IBAN : BE26 0682 2405 0829
BIC : GKCCBEBB
