Les différents cas à rencontrer et le taux de TVA + mention légale à indiquer sur factures :
 
1. Prestation de service :
- En Belgique, 21% de TVA
- Dans l’union Européenne : pas de TVA si ton client est assujetti à la TVA dans son pays + mention légale : « TVA belge non applicable en vertu de l’article 21 §2  du code TVA». Si ton client n’est pas assujetti dans son pays, alors il faut appliquer 21% de TVA sur ta facture
- En dehors de l’union européenne : pas de TVA + mention TVA : « TVA belge non applicable en vertu de l’article 21 §2 du code TVA »
 
2. Livraisons de biens :
- En Belgique : 21% de TVA
- Dans l’union européenne : pas de TVA si ton client est assujetti à la TVA dans son pays + mention légale «  exempté de TVA en vertu de l’article 39 bis du code TVA » à la condition qu’il y ait une facture pour le transport du bien. Si ton client n’est pas assujetti ou s’il n’y a pas de facture de transport alors il faut appliquer 21 % de TVA belge sur ta facture.
- En dehors de l’union européenne : pas de TVA + mention légale « Exempté de TVA en vertu de l’article 39 du code TVA »
 
3 Droits d’auteurs (Royalties) :
- En Belgique : 6% de TVA  à appliquer + mention légale  « Taux réduit à 6% selon l’arrété royal n°20 Tableau A XXIX 2° » 
- Dans l’Union Européenne : pas de TVA si ton client est assujetti + mention légale «  TVA belge non applicable en vertu de l’article 21 §2 ». Si ton client est non assujetti dans son pays, tu dois appliquer les 6% de TVA
- En dehors de l’Union Européenne :   pas de TVA + mention TVA : « TVA belge non applicable en vertu de l’article 21 §2 du code TVA »

4. Refacturation de frais :
- Pas de TVA à appliquer sur la facture si la refacturation de frais concerne des frais pour lesquels aucune TVA n'a été récupérée (dans les achats).
